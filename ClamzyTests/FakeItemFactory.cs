﻿using Clamzy.Adapters;
using FakeItEasy;

namespace ClamzyTests
{
    internal static class FakeItemFactory
    {
        public static Item CreateFakeItem(int id, string name)
        {
            var item = A.Fake<Item>();

            A.CallTo(() => item.Id).Returns(id);
            A.CallTo(() => item.Name).Returns(name);

            return item;
        }
    }
}
