﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;
using Clamzy.Helpers;

namespace ClamzyTests
{
    [TestClass]
    public class CacheTests
    {
        [TestMethod]
        public void CacheShouldReturnOriginalObject()
        {
            var cache = new Cache();            
            var dynObj = cache.GetOrStore("foo", () => new TestCacheObject());

            Thread.Sleep(1000);

            var dynObjFromCache = cache.GetOrStore("foo", () => new TestCacheObject());
            dynObj.Time.ShouldBeEquivalentTo(dynObjFromCache.Time);
        }

        [TestMethod]
        public void CacheShouldReturnNewObjectBecauseOfExpiredDuration()
        {
            var cache = new Cache();
            var dynObj = cache.GetOrStore("foo", () => new TestCacheObject());

            Thread.Sleep(3000);

            var dynObjFromCache = cache.GetOrStore("foo", () => new TestCacheObject(), 1);
            dynObj.Time.Should().NotBe(dynObjFromCache.Time);
        }
        
        [TestMethod]
        public void CacheShouldReturnNewObjectBecauseOfDifferentKey()
        {
            var cache = new Cache();
            var dynObj = cache.GetOrStore("foo", () => new TestCacheObject());

            Thread.Sleep(1000);

            var dynObjFromCache = cache.GetOrStore("foo2", () => new TestCacheObject());
            dynObj.Time.Should().NotBe(dynObjFromCache.Time);
        }

        [TestMethod]
        public void CacheShouldReturnNewObjectBecauseOfCacheKeyClear()
        {
            var cache = new Cache();
            var dynObj = cache.GetOrStore("foo", () => new TestCacheObject());

            cache.RemoveFromCache("foo");

            Thread.Sleep(1000);

            var dynObjFromCache = cache.GetOrStore("foo", () => new TestCacheObject());
            dynObj.Time.Should().NotBe(dynObjFromCache.Time);
        }

        private class TestCacheObject
        {
            public TestCacheObject()
            {
                this.Time = DateTime.UtcNow;
            }
            
            public DateTime Time { get; private set; }
        }
    }    
}
