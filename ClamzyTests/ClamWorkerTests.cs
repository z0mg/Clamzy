﻿using System.Collections.Generic;
using FakeItEasy;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clamzy.Repositories;
using Clamzy.Workers;
using Clamzy.Wrappers;
using Clamzy.Adapters;

namespace ClamzyTests
{
    [TestClass]
    public class ClamWorkerTests
    {
        private IItemRepository itemRepository;
        private IObjectManager objectManager;
        private ISettingRepository settingRepository;

        [TestInitialize]
        public void Initialize()
        {
            this.itemRepository = A.Fake<IItemRepository>();
            this.objectManager = A.Fake<IObjectManager>();
            this.settingRepository = A.Fake<ISettingRepository>();
        }

        [TestMethod]
        public void CanCreate()
        {
            var clamWorker = new ClamWorker(this.itemRepository, this.objectManager, this.settingRepository);
            clamWorker.Should().NotBeNull();
        }
        
        [TestMethod]
        public void ShouldNotCallGetOpenableItemsIfWeAreNotInGame()
        {
            A.CallTo(() => this.objectManager.WeAreInGame()).Returns(false);

            var clamWorker = new ClamWorker(this.itemRepository, this.objectManager, this.settingRepository);

            clamWorker.DoWork();

            A.CallTo(() => this.objectManager.WeAreInGame()).MustHaveHappened();
            A.CallTo(() => this.itemRepository.GetOpenableItems()).MustNotHaveHappened();
        }

        [TestMethod]
        public void ShouldNotCallGetOpenableItemsIfWeAreInGameButNoFreeSlotAvailable()
        {
            A.CallTo(() => this.objectManager.WeAreInGame()).Returns(true);
            A.CallTo(() => this.objectManager.HasFreeBagSpace()).Returns(false);
            
            var clamWorker = new ClamWorker(this.itemRepository, this.objectManager, this.settingRepository);

            clamWorker.DoWork();

            A.CallTo(() => this.objectManager.WeAreInGame()).MustHaveHappened();
            A.CallTo(() => this.itemRepository.GetOpenableItems()).MustNotHaveHappened();
        }

        [TestMethod]
        public void ShouldCallGetOpenableItemsIfWeAreInGameAndThereIsAFreeSlot()
        {
            A.CallTo(() => this.objectManager.WeAreInGame()).Returns(true);
            A.CallTo(() => this.objectManager.HasFreeBagSpace()).Returns(true);

            var clamWorker = new ClamWorker(this.itemRepository, this.objectManager, this.settingRepository);

            clamWorker.DoWork();

            A.CallTo(() => this.objectManager.WeAreInGame()).MustHaveHappened();
            A.CallTo(() => this.itemRepository.GetOpenableItems()).MustHaveHappened();
        }

        [TestMethod]
        public void ShouldCallUsableOnItemButNotUseWhenWeAreInGameWithAFreeSlotWithAFreeSlotAndHaveOpenableItemThatCannotBeUsed()
        {
            var fakeItem = FakeItemFactory.CreateFakeItem(12345, "Magic Clam");

            A.CallTo(() => this.objectManager.WeAreInGame()).Returns(true);
            A.CallTo(() => this.objectManager.HasFreeBagSpace()).Returns(true);
            A.CallTo(() => this.itemRepository.GetOpenableItems()).Returns(new List<Item> { fakeItem });

            var clamWorker = new ClamWorker(this.itemRepository, this.objectManager, this.settingRepository);

            clamWorker.DoWork();

            A.CallTo(() => this.objectManager.WeAreInGame()).MustHaveHappened();
            A.CallTo(() => this.itemRepository.GetOpenableItems()).MustHaveHappened();
            A.CallTo(() => fakeItem.CanUse()).MustHaveHappened();
            A.CallTo(() => fakeItem.Use()).MustNotHaveHappened();
        }

        [TestMethod]
        public void ShouldCallUsableOnItemWhenWeAreInGameWithAFreeSlotAndHaveOpenableItem()
        {
            var fakeItem = FakeItemFactory.CreateFakeItem(12345, "Magic Clam");

            A.CallTo(() => fakeItem.CanUse()).Returns(false);          
            A.CallTo(() => this.objectManager.WeAreInGame()).Returns(true);
            A.CallTo(() => this.objectManager.HasFreeBagSpace()).Returns(true);
            A.CallTo(() => this.itemRepository.GetOpenableItems()).Returns(new List<Item> { fakeItem });

            var clamWorker = new ClamWorker(this.itemRepository, this.objectManager, this.settingRepository);

            clamWorker.DoWork();

            A.CallTo(() => this.objectManager.WeAreInGame()).MustHaveHappened();
            A.CallTo(() => this.itemRepository.GetOpenableItems()).MustHaveHappened();
            A.CallTo(() => fakeItem.CanUse()).MustHaveHappened();
            A.CallTo(() => fakeItem.Use()).MustNotHaveHappened();
        }

        [TestMethod]
        public void ShouldCallUsableOnlyOnFirstItemWhenWeAreInGameWithAFreeSlotAndHave2OpenableItemsOnSingleCallToLookHuman()
        {
            var fakeItem = FakeItemFactory.CreateFakeItem(12345, "Magic Clam");
            var otherFakeItem = FakeItemFactory.CreateFakeItem(12345, "Magic Clam");

            A.CallTo(() => fakeItem.CanUse()).Returns(true);
            A.CallTo(() => otherFakeItem.CanUse()).Returns(true);
            A.CallTo(() => this.objectManager.WeAreInGame()).Returns(true);
            A.CallTo(() => this.objectManager.HasFreeBagSpace()).Returns(true);
            A.CallTo(() => this.itemRepository.GetOpenableItems()).Returns(new List<Item> { fakeItem, otherFakeItem });

            var clamWorker = new ClamWorker(this.itemRepository, this.objectManager, this.settingRepository);

            clamWorker.DoWork();

            A.CallTo(() => this.objectManager.WeAreInGame()).MustHaveHappened();
            A.CallTo(() => this.itemRepository.GetOpenableItems()).MustHaveHappened();
            A.CallTo(() => fakeItem.CanUse()).MustHaveHappened();
            A.CallTo(() => fakeItem.Use()).MustHaveHappened();
            A.CallTo(() => otherFakeItem.CanUse()).MustNotHaveHappened();
            A.CallTo(() => otherFakeItem.Use()).MustNotHaveHappened();
        }

        [TestMethod]
        public void ShouldCallUsableOnBothItemsWhenWeAreInGameWithAFreeSlotAndHave2OpenableItemsOnTwoCalls()
        {
            var fakeItem = FakeItemFactory.CreateFakeItem(12345, "Magic Clam");
            var otherFakeItem = FakeItemFactory.CreateFakeItem(12345, "Magic Clam");

            A.CallTo(() => fakeItem.CanUse()).Returns(true);
            A.CallTo(() => otherFakeItem.CanUse()).Returns(true);
            A.CallTo(() => this.objectManager.WeAreInGame()).Returns(true);
            A.CallTo(() => this.objectManager.HasFreeBagSpace()).Returns(true);
            A.CallTo(() => this.itemRepository.GetOpenableItems()).Returns(new List<Item> { fakeItem, otherFakeItem });

            var clamWorker = new ClamWorker(this.itemRepository, this.objectManager, this.settingRepository);

            clamWorker.DoWork();

            A.CallTo(() => this.objectManager.WeAreInGame()).MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => this.itemRepository.GetOpenableItems()).MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => fakeItem.CanUse()).MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => fakeItem.Use()).MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => otherFakeItem.CanUse()).MustNotHaveHappened();
            A.CallTo(() => otherFakeItem.Use()).MustNotHaveHappened();

            A.CallTo(() => this.itemRepository.GetOpenableItems()).Returns(new List<Item> { otherFakeItem }); // fakeItem is now gone/opened.

            clamWorker.DoWork();

            A.CallTo(() => this.objectManager.WeAreInGame()).MustHaveHappened(Repeated.Exactly.Twice);
            A.CallTo(() => this.itemRepository.GetOpenableItems()).MustHaveHappened(Repeated.Exactly.Twice);
            A.CallTo(() => fakeItem.CanUse()).MustHaveHappened(Repeated.Exactly.Once); // Original loop.
            A.CallTo(() => fakeItem.Use()).MustHaveHappened(Repeated.Exactly.Once); // Original loop.
            A.CallTo(() => otherFakeItem.CanUse()).MustHaveHappened(Repeated.Exactly.Once); // New loop.
            A.CallTo(() => otherFakeItem.Use()).MustHaveHappened(Repeated.Exactly.Once); // New loop.
        }
    }
}
