﻿using FakeItEasy;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clamzy.Constants;
using Clamzy.Repositories;
using Clamzy.Wrappers;

namespace ClamzyTests
{
    [TestClass]
    public class SettingsRepositoryTests
    {
        private IOptionManager optionManager;

        [TestInitialize]
        public void Initialize()
        {
            this.optionManager = A.Fake<IOptionManager>();
        }

        [TestMethod]
        public void GetClamStringsReturnsNullWhenNotSet()
        {
            var settingRepository = new SettingRepository(this.optionManager);

            settingRepository.GetClamStrings().Should().BeNull();
        }

        [TestMethod]
        public void GetClamStringsReturnsTheLastSet()
        {
            var settingRepository = new SettingRepository(this.optionManager);

            settingRepository.AddSetting(SettingKey.ClamStrings, new[] { "MyClamString" });
            settingRepository.GetClamStrings().Should().BeEquivalentTo("MyClamString");
        }

        [TestMethod]
        public void GetClamStringsReturnsTheLastSetMultipleLines()
        {
            var settingRepository = new SettingRepository(this.optionManager);

            settingRepository.AddSetting(SettingKey.ClamStrings, new[] { "MyClamString", "OtherClamString" });
            settingRepository.GetClamStrings().Should().BeEquivalentTo("MyClamString", "OtherClamString");
        }

        [TestMethod]
        public void GetClamStringsReturnsNullWhenSingleStringCalled()
        {
            var settingRepository = new SettingRepository(this.optionManager);

            settingRepository.AddSetting(SettingKey.ClamStrings, "MyClamString");
            settingRepository.GetClamStrings().Should().BeNull();
        }

        [TestMethod]
        public void CanLoadSettingsFromJsonFile()
        {
            A.CallTo(() => this.optionManager.LoadFromJson<string[]>(A<string>.That.IsEqualTo("ClamzySettings-ClamStrings"))).Returns(new[]
            {
                "12345",
                "Clam",
                "67890",
                "Box"
            });

            var settingRepository = new SettingRepository(this.optionManager);
            settingRepository.LoadSetting(SettingKey.ClamStrings);
            settingRepository.GetClamStrings().Length.Should().Be(4);
            settingRepository.GetClamStrings()[0].ShouldBeEquivalentTo("12345");
            settingRepository.GetClamStrings()[1].ShouldBeEquivalentTo("Clam");
            settingRepository.GetClamStrings()[2].ShouldBeEquivalentTo("67890");
            settingRepository.GetClamStrings()[3].ShouldBeEquivalentTo("Box");
        }
    }
}
