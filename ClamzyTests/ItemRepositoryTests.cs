﻿using FakeItEasy;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clamzy.Helpers;
using Clamzy.Repositories;
using Clamzy.Wrappers;
using Clamzy.Adapters;

namespace ClamzyTests
{
    [TestClass]
    public class ItemRepositoryTests
    {
        private ICache cache;
        private IObjectManager objectManager;
        private ISettingRepository settingRepository;

        [TestInitialize]
        public void Initialize()
        {
            this.cache = new CacheInvoker();
            this.objectManager = A.Fake<IObjectManager>();
            this.settingRepository = A.Fake<ISettingRepository>();
        }

        [TestMethod]
        public void CanCreate()
        {
            new ItemRepository(this.cache, this.objectManager, this.settingRepository).Should().NotBeNull();
        }

        [TestMethod]
        public void ShouldClearCache()
        {
            var fakeCache = A.Fake<ICache>();

            var itemRepository = new ItemRepository(fakeCache, this.objectManager, this.settingRepository);
            itemRepository.ClearCachedData();

            A.CallTo(() => fakeCache.RemoveFromCache("itemFilters")).MustHaveHappened();
        }

        [TestMethod]
        public void OpenableItemsShouldBeEmptyWhenNoStringSet()
        {
            var itemRepository = new ItemRepository(this.cache, this.objectManager, this.settingRepository);
            itemRepository.GetOpenableItems().Should().BeEmpty();
        }

        [TestMethod]
        public void OpenableItemsShouldBeEmptyWhenStringSetButNoMatchingItems()
        {
            A.CallTo(() => this.settingRepository.GetClamStrings()).Returns(new[] { "12345", "67890" });
            A.CallTo(() => this.objectManager.GetWoWItems()).Returns(new Item[] { });

            var itemRepository = new ItemRepository(this.cache, this.objectManager, this.settingRepository);

            itemRepository.GetOpenableItems().Should().BeEmpty();
        }

        [TestMethod]
        public void OpenableItemsShouldContainOneWhenOneMatchingId()
        {
            A.CallTo(() => this.settingRepository.GetClamStrings()).Returns(new[] { "12345", "67890" });

            var firstItem = A.Fake<Item>();
            A.CallTo(() => firstItem.Id).Returns(12345);
            A.CallTo(() => firstItem.Name).Returns("Dirty Clam");


            A.CallTo(() => this.objectManager.GetWoWItems()).Returns(new Item[] { FakeItemFactory.CreateFakeItem(12345, "Dirty Clam"), FakeItemFactory.CreateFakeItem(23456, "Glorious Weapon of Death") });

            var itemRepository = new ItemRepository(this.cache, this.objectManager, this.settingRepository);

            itemRepository.GetOpenableItems().Should().NotBeEmpty();
            itemRepository.GetOpenableItems().Count.Should().Be(1);
        }

        [TestMethod]
        public void OpenableItemsShouldContainOneWhenOneMatchingName()
        {
            A.CallTo(() => this.settingRepository.GetClamStrings()).Returns(new[] { "dirty clam", "67890" });

            var firstItem = A.Fake<Item>();
            A.CallTo(() => firstItem.Id).Returns(12345);
            A.CallTo(() => firstItem.Name).Returns("Dirty Clam");


            A.CallTo(() => this.objectManager.GetWoWItems()).Returns(new Item[] { FakeItemFactory.CreateFakeItem(12345, "Dirty Clam"), FakeItemFactory.CreateFakeItem(23456, "Glorious Weapon of Death") });

            var itemRepository = new ItemRepository(this.cache, this.objectManager, this.settingRepository);

            itemRepository.GetOpenableItems().Should().NotBeEmpty();
            itemRepository.GetOpenableItems().Count.Should().Be(1);
        }

        [TestMethod]
        public void OpenableItemsShouldContainTwoWhenOneMatchingNameAndOneMatchingId()
        {
            A.CallTo(() => this.settingRepository.GetClamStrings()).Returns(new[] { "dirty clam", "67890" });

            var firstItem = A.Fake<Item>();
            A.CallTo(() => firstItem.Id).Returns(12345);
            A.CallTo(() => firstItem.Name).Returns("Dirty Clam");


            A.CallTo(() => this.objectManager.GetWoWItems()).Returns(new Item[] { FakeItemFactory.CreateFakeItem(12345, "Dirty Clam"), FakeItemFactory.CreateFakeItem(67890, "Glorious Weapon of Death") });

            var itemRepository = new ItemRepository(this.cache, this.objectManager, this.settingRepository);

            itemRepository.GetOpenableItems().Should().NotBeEmpty();
            itemRepository.GetOpenableItems().Count.Should().Be(2);
        }
    }
}

