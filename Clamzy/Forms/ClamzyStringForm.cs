﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Clamzy.Forms
{
    internal partial class ClamzyStringForm : Form
    {
        public string ClamString { get; private set; }

        public ClamzyStringForm(string clamString)
        {
            this.InitializeComponent();

            this.txtBoxClamString.Text = clamString;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.ClamString = this.txtBoxClamString.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}