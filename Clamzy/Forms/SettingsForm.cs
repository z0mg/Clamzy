﻿using System;
using System.Linq;
using System.Windows.Forms;
using Clamzy.Constants;
using Clamzy.Repositories;

namespace Clamzy.Forms
{
    internal partial class SettingsForm : Form
    {
        private readonly ISettingRepository settingRepository;
        private readonly IItemRepository itemRepository;
        
        public SettingsForm(ISettingRepository settingRepository, IItemRepository itemRepository)
        {
            this.settingRepository = settingRepository;
            this.itemRepository = itemRepository;

            this.InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            using (ClamzyStringForm dialog = new ClamzyStringForm(this.txtBoxClamString.Text))
            {
                if (dialog.ShowDialog(this) == DialogResult.OK)
                {
                    this.txtBoxClamString.Text = dialog.ClamString;
                    this.settingRepository.AddSetting(SettingKey.ClamStrings, dialog.ClamString.Split(new[] { Environment.NewLine }, StringSplitOptions.None).Where(ts => string.IsNullOrWhiteSpace(ts) == false).ToArray());
                    this.itemRepository.ClearCachedData();
                }
            }
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            var clamStrings = this.settingRepository.GetClamStrings();

            if (clamStrings != null)
            {
                foreach (var str in clamStrings)
                {
                    this.txtBoxClamString.Text += str + Environment.NewLine;
                }
            }
        }
    }
}