﻿using System;

namespace Clamzy.Models
{
    internal class CacheItem
    {
        public object StoredObject { get; private set; }
        public DateTime Time { get; private set; }

        public CacheItem(object obj)
        {
            this.StoredObject = obj;
            this.Time = DateTime.UtcNow;
        }
    }
}
