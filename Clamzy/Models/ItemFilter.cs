﻿namespace Clamzy.Models
{
    public class ItemFilter
    {
        public ItemFilter(int itemId)
        {
            this.IsIdFilter = true;
            this.ItemId = itemId;
        }

        public ItemFilter(string name)
        {
            this.IsNameFilter = true;
            this.ItemName = name;
        }

        public bool IsIdFilter { get; private set; }
        public bool IsNameFilter { get; private set; }
        public int ItemId { get; private set; }
        public string ItemName { get; private set; }
    }
}
