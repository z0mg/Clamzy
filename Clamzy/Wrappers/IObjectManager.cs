﻿using Clamzy.Adapters;
using ZzukBot.Objects;

namespace Clamzy.Wrappers
{
    internal interface IObjectManager
    {
        bool WeAreInGame();
        bool HasFreeBagSpace();
        Item[] GetWoWItems();
    }
}
