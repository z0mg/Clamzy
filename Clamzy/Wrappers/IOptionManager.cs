﻿namespace Clamzy.Wrappers
{
    internal interface IOptionManager
    {
        void SaveToJson(string key, object value);
        T LoadFromJson<T>(string key);
    }
}
