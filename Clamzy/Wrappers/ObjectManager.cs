﻿using Clamzy.Adapters;
using System.Linq;

namespace Clamzy.Wrappers
{
    internal class ObjectManager : IObjectManager
    {
        public Item[] GetWoWItems()
        {
            if (this.WeAreInGame() && ZzukBot.Game.Statics.ObjectManager.Instance.Items != null)
            {
                return ZzukBot.Game.Statics.ObjectManager.Instance.Items.Select(i => new Item(i)).ToArray();
            }

            return new Item[0];
        }

        public bool WeAreInGame()
        {
            return ZzukBot.Game.Statics.ObjectManager.Instance != null && ZzukBot.Game.Statics.ObjectManager.Instance.Player != null && ZzukBot.Game.Statics.ObjectManager.Instance.Player.Level > 0 && ZzukBot.Game.Statics.ObjectManager.Instance.IsIngame;
        }

        public bool HasFreeBagSpace()
        {
            return this.WeAreInGame() && ZzukBot.Game.Statics.Inventory.Instance.CountFreeSlots(false) > 0;
        }
    }
}