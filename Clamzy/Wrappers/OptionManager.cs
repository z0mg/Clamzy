﻿namespace Clamzy.Wrappers
{
    internal class OptionManager : IOptionManager
    {
        public void SaveToJson(string key, object value)
        {
            var optManager = ZzukBot.Settings.OptionManager.Get(key);
            if (value != null && optManager != null)
            {
                optManager.SaveToJson(value);
            }
        }

        public T LoadFromJson<T>(string key)
        {
            var optManager = ZzukBot.Settings.OptionManager.Get(key);
            if (optManager == null)
            {
                return default(T);
            }

            return optManager.LoadFromJson<T>();
        }
    }
}