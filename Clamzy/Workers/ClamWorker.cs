﻿using Clamzy.Repositories;
using Clamzy.Wrappers;

namespace Clamzy.Workers
{
    internal class ClamWorker : IWorker
    {
        private readonly IItemRepository itemRepository;
        private readonly IObjectManager objectManager;
        private readonly ISettingRepository settingRepository;
        
        public ClamWorker(IItemRepository itemRepository, IObjectManager objectManager, ISettingRepository settingRepository)
        {
            this.itemRepository = itemRepository;
            this.objectManager = objectManager;
            this.settingRepository = settingRepository;
        }

        public void DoWork()
        {
            if (this.objectManager.WeAreInGame() == false)
            {
                return;
            }

            if (this.objectManager.HasFreeBagSpace() == false)
            {
                return;
            }

            var openableItems = this.itemRepository.GetOpenableItems();
           
            foreach (var item in openableItems)
            {
                if (item.CanUse())
                {
                    item.Use();
                    break;
                }
            }
        }
    }
}
