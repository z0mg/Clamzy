﻿namespace Clamzy.Workers
{
    internal interface IWorker
    {
        void DoWork();
    }
}
