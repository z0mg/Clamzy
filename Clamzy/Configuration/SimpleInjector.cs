﻿using SimpleInjector;
using System;
using Clamzy.Forms;
using Clamzy.Helpers;
using Clamzy.Repositories;
using Clamzy.Workers;
using Clamzy.Wrappers;

namespace Clamzy.Configuration
{
    internal class SimpleInjector
    {
        private static readonly Lazy<Container> ContainerInstance = new Lazy<Container>(() => new Container());

        private bool hasInitialized;

        public Container Container { get { return ContainerInstance.Value; } }

        public void Initialize()
        {
            if (this.hasInitialized == false)
            {
                this.hasInitialized = true;
                // Helpers
                this.Container.Register<ICache, Cache>(Lifestyle.Singleton);

                // Wrappers;
                this.Container.Register<IObjectManager, ObjectManager>(Lifestyle.Singleton);
                this.Container.Register<IOptionManager, OptionManager>(Lifestyle.Singleton);

                // Repositories
                this.Container.Register<IItemRepository, ItemRepository>(Lifestyle.Singleton);
                this.Container.Register<ISettingRepository, SettingRepository>(Lifestyle.Singleton);

                // Workers
                this.Container.Register<IWorker, ClamWorker>(Lifestyle.Transient);

                // Forms
                this.Container.Register<SettingsForm>(Lifestyle.Singleton);
            }

            // Check
            this.Container.Verify();
        }
    }
}