﻿using System;
using System.ComponentModel.Composition;
using System.Threading;
using System.Threading.Tasks;
using Clamzy.Constants;
using Clamzy.Forms;
using Clamzy.Repositories;
using Clamzy.Workers;
using ZzukBot.ExtensionFramework.Interfaces;

namespace Clamzy
{
    [Export(typeof(IPlugin))]
    public class Clamzy : IPlugin
    {
        private static readonly Lazy<Configuration.SimpleInjector> simpleInjectorInstance = new Lazy<Configuration.SimpleInjector>(() => new Configuration.SimpleInjector());

        private Configuration.SimpleInjector Injector  { get { return simpleInjectorInstance.Value; } }

        private SettingsForm settingsForm;

        private CancellationTokenSource cancellationToken;

        private Random random;

        public Clamzy()
        {
            this.Injector.Initialize();
        }

        public string Author
        {
            get
            {
                return "z0mg";
            }
        }

        public string Name
        {
            get
            {
                return "Clamzy";
            }
        }

        public int Version
        {
            get
            {
                return 1;
            }
        }

        public bool Load()
        {
            this.cancellationToken = new CancellationTokenSource();
            this.random = new Random();

            this.LoadSettings();

            Task.Run(async () =>
            {
                while (this.cancellationToken.IsCancellationRequested == false)
                {
                    var clamWorker = this.Injector.Container.GetInstance<IWorker>();
                    clamWorker.DoWork();
                    await Task.Delay(random.Next(8000, 15000));
                }
            });

            return true; 
        }
        
        public void ShowGui()
        {         
            if (this.settingsForm != null && this.settingsForm.Visible)
            {
                return;
            }

            this.settingsForm = this.Injector.Container.GetInstance<SettingsForm>();
            this.settingsForm.ShowDialog();
        }

        public void Unload()
        {         
            if (this.settingsForm == null)
            {
                return;
            }

            this.settingsForm.Close();
            this.settingsForm.Dispose();
            this.settingsForm = null;

            this.cancellationToken.Cancel();
        }

        public void _Dispose()
        {
            this.Unload(); 
        }

        private void LoadSettings()
        {
            var settingRepository = this.Injector.Container.GetInstance<ISettingRepository>();
            settingRepository.LoadSetting(SettingKey.ClamStrings);
        }
    }
}
