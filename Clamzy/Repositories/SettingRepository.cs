﻿using System;
using System.Collections;
using Clamzy.Constants;
using Clamzy.Wrappers;

namespace Clamzy.Repositories
{
    internal class SettingRepository : ISettingRepository
    {
        private const string SettingsKey = "ClamzySettings";

        private readonly IOptionManager optionManager;

        private readonly Hashtable settings;

        public SettingRepository(IOptionManager optionManager)
        {
            this.optionManager = optionManager;
            this.settings = new Hashtable();
        }

        public void AddSetting(SettingKey key, string value)
        {
            if (this.settings.ContainsKey(key) == false)
            {
                this.settings.Add(key, value);
            }
            else
            {
                this.settings[key] = value;
            }

            this.SaveSettingsToFile(key);
        }

        public void AddSetting(SettingKey key, string[] values)
        {
            if (this.settings.ContainsKey(key) == false)
            {
                this.settings.Add(key, values);
            }
            else
            {
                this.settings[key] = values;
            }

            this.SaveSettingsToFile(key);
        }

        public string[] GetClamStrings()
        {
            if (this.settings.ContainsKey(SettingKey.ClamStrings))
            {
                return this.settings[SettingKey.ClamStrings] as string[];
            }

            return null;
        }

        public void LoadSetting(SettingKey key)
        {
            switch (key)
            {
                case SettingKey.ClamStrings:
                    var clamStrings = this.optionManager.LoadFromJson<string[]>(this.GetSettingsKeyStringForKey(key));
                    if (clamStrings != null)
                    {
                        this.AddSetting(key, clamStrings);
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException("key", key, null);
            }
        }

        private void SaveSettingsToFile(SettingKey key)
        {
            switch (key)
            {
                case SettingKey.ClamStrings:
                    var talentStrings = this.GetClamStrings();
                    if (talentStrings != null)
                    {
                        this.optionManager.SaveToJson(this.GetSettingsKeyStringForKey(key), talentStrings);
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException("key", key, null);
            }
        }

        private string GetSettingsKeyStringForKey(SettingKey key)
        {
            return string.Format("{0}-{1}", SettingsKey, key);
        }
    }
}
