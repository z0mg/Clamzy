﻿using System;
using System.Collections.Generic;
using Clamzy.Helpers;
using Clamzy.Models;
using Clamzy.Wrappers;
using Clamzy.Adapters;

namespace Clamzy.Repositories
{
    internal class ItemRepository : IItemRepository
    {
        private const string KeyItemFilters = "itemFilters";

        private readonly ICache cache;
        private readonly IObjectManager objectManager;
        private readonly ISettingRepository settingRepository;
        
        public ItemRepository(ICache cache, IObjectManager objectManager, ISettingRepository settingRepository)
        {
            this.cache = cache;
            this.objectManager = objectManager;
            this.settingRepository = settingRepository;
        }

        public IReadOnlyList<Item> GetOpenableItems()
        {
            var openableItems = new List<Item>();
            var allItems = this.objectManager.GetWoWItems();
            var itemFilters = this.cache.GetOrStore(KeyItemFilters, this.GetItemFilters, 600);

            foreach (var item in allItems)
            {
                foreach (var filter in itemFilters)
                {
                    if (filter.IsIdFilter && item.Id.Equals(filter.ItemId))
                    {
                        openableItems.Add(item);
                        break;
                    }

                    if (filter.IsNameFilter && item.Name.EndsWith(filter.ItemName, StringComparison.OrdinalIgnoreCase))
                    {
                        openableItems.Add(item);
                        break;
                    }
                }
            }

            return openableItems;
        }


        public void ClearCachedData()
        {
            this.cache.RemoveFromCache(KeyItemFilters);
        }

        private IReadOnlyList<ItemFilter> GetItemFilters()
        {
            var itemList = new List<ItemFilter>();
            var clamStrings = this.settingRepository.GetClamStrings();

            foreach (var str in clamStrings)
            {
                int itemId;
                if (int.TryParse(str, out itemId))
                {
                    itemList.Add(new ItemFilter(itemId));
                }
                else
                {
                    itemList.Add(new ItemFilter(str));
                }
            }

            return itemList;
        }
    }
}
