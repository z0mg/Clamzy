﻿using System.Collections.Generic;
using Clamzy.Adapters;

namespace Clamzy.Repositories
{
    internal interface IItemRepository
    {
        IReadOnlyList<Item> GetOpenableItems();
        void ClearCachedData();
    }
}
