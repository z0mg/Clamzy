﻿using Clamzy.Constants;

namespace Clamzy.Repositories
{
    internal interface ISettingRepository
    {           
        void AddSetting(SettingKey key, string value);
        void AddSetting(SettingKey key, string[] values);
        string[] GetClamStrings();
        void LoadSetting(SettingKey key);
    }
}