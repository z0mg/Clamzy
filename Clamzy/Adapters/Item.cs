﻿using ZzukBot.Objects;

namespace Clamzy.Adapters
{
    internal class Item
    {
        private readonly WoWItem wrapped;

        public Item()
        {
        }

        public Item(WoWItem itemToWrap)
        {
            this.wrapped = itemToWrap;
        }

        public virtual int Id { get { return this.wrapped.Id; } }
        public virtual string Name { get { return this.wrapped.Name; } }

        public virtual bool CanUse()
        {
            return this.wrapped.CanUse();
        }

        public virtual void Use()
        {
            this.wrapped.Use();
        }
    }
}
