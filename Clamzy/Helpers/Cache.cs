﻿using System;
using System.Collections;
using Clamzy.Models;

namespace Clamzy.Helpers
{
    internal class Cache : ICache
    {
        private readonly object lockert = new object();
        private readonly Hashtable cache;

        public Cache()
        {
            this.cache = new Hashtable();
        }
        
        public T GetOrStore<T>(string key, Func<T> action, int maxDuration = -1) where T : class
        {
            var result = this.cache[key];

            if (result == null ||
                (maxDuration > 0 && DateTime.UtcNow > ((CacheItem)result).Time.AddSeconds(maxDuration)))
            {
                lock (lockert)
                {
                    if (result == null ||
                        (maxDuration > 0 && DateTime.UtcNow > ((CacheItem)result).Time.AddSeconds(maxDuration)))
                    {
                        var obj = action();
                        result = obj != null ? new CacheItem(obj) : new CacheItem(default(T));
                        this.cache[key] = result;
                    }
                }
            }

            if (result == null)
            {
                return default(T);
            }

            return (T)((CacheItem)result).StoredObject;
        }

        public void RemoveFromCache(string key)
        {
            if (this.cache.ContainsKey(key))
            {
                this.cache.Remove(key);
            }
        }
    }
}
