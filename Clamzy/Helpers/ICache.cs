﻿using System;

namespace Clamzy.Helpers
{
    internal interface ICache
    {
        T GetOrStore<T>(string key, Func<T> action, int maxDuration = -1) where T : class;
        void RemoveFromCache(string key);
    }
}
